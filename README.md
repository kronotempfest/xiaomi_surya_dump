# qssi-user 12 SKQ1.211019.001 V14.0.2.0.SJGMIXM release-keys
- manufacturer: xiaomi
- platform: sm6150
- codename: surya
- flavor: qssi-user
- release: 12
- id: SKQ1.211019.001
- incremental: V14.0.2.0.SJGMIXM
- tags: release-keys
- fingerprint: POCO/surya_global/surya:12/RKQ1.211019.001/V14.0.2.0.SJGMIXM:user/release-keys
POCO/karna_global/karna:12/RKQ1.211019.001/V14.0.2.0.SJGMIXM:user/release-keys
POCO/surya_global/surya:12/RKQ1.211019.001/V14.0.2.0.SJGMIXM:user/release-keys
- is_ab: false
- brand: POCO
- branch: qssi-user-12-SKQ1.211019.001-V14.0.2.0.SJGMIXM-release-keys
- repo: poco_surya_dump
